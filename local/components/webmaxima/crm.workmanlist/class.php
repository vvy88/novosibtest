<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class WebmaximaCrmWorkmanlist extends \CBitrixComponent {

    public function executeComponent() {
    	//получаем исполнителей
		$this->arResult['WORKMAN']=[];
		$resUser = \Bitrix\Main\UserTable::getList([
			"filter" => [
				"ACTIVE"=>"Y"
			],
			"order"=>['ID'=>'ASC'],
			"select" => [
				'ID',
				'NAME',
				'LAST_NAME',
				'UF_CRM_POSITION'
			]
		]);
		while ($arUser = $resUser->fetch()) {
			$this->arResult['WORKMAN'][$arUser['ID']]=$arUser;
		}

		//получаем роли
		$this->arResult['USER_POSITION']=[];
		$arPositionField = CUserTypeEntity::GetList(
			[],
			[
				"FIELD_NAME"=>"UF_CRM_POSITION",
				"ENTITY_ID"=>"USER"
			]
		)->Fetch();
		$resPosition = CUserFieldEnum::GetList(
			["SORT"=>"ASC"],
			["USER_FIELD_ID" => $arPositionField["ID"]]
		);
		while($arPosition = $resPosition->Fetch()) {
			$this->arResult['USER_POSITION'][$arPosition['ID']]=$arPosition;
		}


		$this->IncludeComponentTemplate();

        return $this->arResult;
    }

}