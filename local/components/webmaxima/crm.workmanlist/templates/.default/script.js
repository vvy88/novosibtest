$(document).ready(function(){
	//создание
	$(document).on('click', '#workmanAdd', function(){
		new jBox('Modal')
			.setTitle('Создать исполнителя')
			.open({
				ajax : {url : $(this).attr('data-href')},
				width: 500,
			});
	});

	//редактирование
	$(document).on('click', '#workmanList .editWorkman', function(){
		new jBox('Modal')
			.setTitle('Редактирование пользователя')
			.open({
				ajax : {url : $(this).attr('data-href')},
				width: 500,
			});

		return false;
	});

	//удаление
	$(document).on('click', '#workmanList .deleteWorkman', function(){
		var link=this;
		$.ajax({
			type: 'GET',
			url: $(this).attr('data-href'),
			dataType: 'json',
			success: function (data) {
				reloadWorkmanList(data);
				if (data.SUCCESS) {
					alert(data.MESSAGE);
				} else {
					alert(data.ERROR.join());
				}
			},
			error: function (xhr, str) {
				alert('Оу, что-то пошло не так, обратитесь в техническую поддержку :(');
			}
		});
		return false;
	});

});

function reloadWorkmanList(data) {
	if (data.SUCCESS) {
		$.ajax({
			type: 'GET',
			url: '/ajax/workmanlist.php',
			success: function (data) {
				$('#workmanListBlock').replaceWith(data)
			},
			error: function (xhr, str) {
				alert('Оу, что-то пошло не так, обратитесь в техническую поддержку :(');
			}
		});
	}
}