<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?
Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/assets/jBox/dist/jBox.all.min.js');
Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/assets/jBox/dist/jBox.all.min.css');
?>

<div id="workmanListBlock">
    <table class="table" id="workmanList">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Имя</th>
            <th scope="col">Должность</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
        <? if (!count($arResult['WORKMAN'])) { ?>
            <tr>
                <td colspan="5">Исполнители не найдены</td>
            </tr>
        <? } else { ?>
            <? foreach ($arResult['WORKMAN'] AS $arUser) { ?>
                <tr class="workmanBlock">
                    <td><?=$arUser['ID']?></td>
                    <td><?=$arUser['NAME']?> <?=$arUser['LAST_NAME']?></td>
                    <td>
                        <?=$arResult['USER_POSITION'][$arUser['UF_CRM_POSITION']]['VALUE']?>
                    </td>
                    <td>
                        <a class="editWorkman" href="#" data-href="/ajax/workmanform.php?ACTION=EDIT_FORM&JS_CALLBACK=reloadWorkmanList&WORKMAN_ID=<?=$arUser['ID']?>">редактировать</a>
                        <a class="deleteWorkman" href="#" data-href="/local/components/webmaxima/crm.workmanform/ajax.php?ACTION=DELETE&WORKMAN_ID=<?=$arUser['ID']?>">удалить</a>
                    </td>
                </tr>
            <? } ?>
        <? } ?>
        </tbody>
    </table>

    <a class="btn btn-primary" href="#" data-href='/ajax/workmanform.php?ACTION=ADD_FORM&JS_CALLBACK=reloadWorkmanList' id="workmanAdd" role="button">Добавить исполнителя</a>
</div>
