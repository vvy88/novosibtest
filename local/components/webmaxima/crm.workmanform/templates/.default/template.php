<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<form id="workmanForm" data-action="<?=$arResult['AJAX_URL']?>" data-jsCallback="<?=$arResult['JS_CALLBACK']?>">
    <div class="message"></div>
    <input type="hidden" name="ACTION" value="<?=$arResult['FORM_VALUES']['ACTION']?>">

    <? if ($arResult['ACTION']=='EDIT_FORM') { ?>
        <input type="hidden" name="WORKMAN_ID" value="<?=$arResult['FORM_VALUES']['WORKMAN_ID']?>">
    <? } ?>

    <div class="row form-group">
        <div class="col-4">
            <label for="addFormName">Имя</label>
        </div>
        <div class="col-8">
            <input type="text" class="form-control" id="addFormName" name="NAME" value="<?=$arResult['FORM_VALUES']['NAME']?>" required placeholder="Имя Фамилия">
        </div>
    </div>

    <? if ($arResult['ACTION']=='ADD_FORM') { ?>
    <div class="row form-group">
        <div class="col-4">
            <label for="addFormEmail">Email/логин</label>
        </div>
        <div class="col-8">
            <input type="text" class="form-control" id="addFormEmail" name="EMAIL" required value="<?=$arResult['FORM_VALUES']['EMAIL']?>">
        </div>
    </div>
    <? } ?>

    <div class="row form-group">
        <div class="col-4">
            <label for="addFormPosition">Должность</label>
        </div>
        <div class="col-8">
            <select class="form-control" id="addFormPosition" name="POSITION" required>
				<? foreach ($arResult['POSITION'] AS $arPosition) { ?>
                    <option
                        value="<?=$arPosition['ID']?>"
						<? if ($arPosition['ID']==$arResult['FORM_VALUES']['POSITION']) {?> selected <?} ?>
                    ><?=$arPosition['VALUE']?></option>
				<? } ?>
            </select>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Сохранить</button>
</form>

<? //не убирать в script.php подгружается через ajax ?>
<script>
	$(document).ready(function(){
		$('#workmanForm').submit(function() {
			var form=this;
			var data   = $(this).serialize();
			$.ajax({
				type: 'POST',
				url: $(this).attr('data-action'),
				data: data,
				dataType: 'json',
				success: function (data) {
					if (typeof window[$(form).attr('data-jsCallback')] == 'function') {
						window[$(form).attr('data-jsCallback')](data);
					}
					if (data.SUCCESS) {
						$(form).replaceWith('<div class="alert alert-success" role="alert">'+data.MESSAGE+'</div>');
					} else {
						$(form).find('.message').html('<div class="alert alert-danger" role="alert">'+data.ERROR.join()+'</div>');
					}
				},
				error: function (xhr, str) {
					$(form).replaceWith('<div class="alert alert-danger" role="alert">Оу, что-то пошло не так, обратитесь в техническую поддержку :(</div>');
				}
			});

			return false;
		});
	});
</script>