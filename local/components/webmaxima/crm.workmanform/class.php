<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class WebmaximaCrmWorkmanForm extends \CBitrixComponent {
    public function executeComponent() {
    	$this->arResult['ACTION']=$this->arParams['ACTION'];
		$this->arResult['AJAX_URL']=$this->GetPath().'/ajax.php';
		$this->arResult['JS_CALLBACK']=$this->arParams['JS_CALLBACK'];

    	if (empty($this->arParams['ACTION']) || $this->arParams['ACTION']=='ADD_FORM') {
			return $this->showAddForm();
		} else if ($this->arParams['ACTION']=='EDIT_FORM') {
			return $this->showEditForm();
		}
    }

	private function showAddForm() {
		$this->arResult['FORM_VALUES']=[
			'ACTION'=>'ADD',
		];
    	return $this->showForm();
	}

	private function showEditForm() {

    	//данные пользователя
		$arUser = \Bitrix\Main\UserTable::getList([
			"filter" => [
				"ACTIVE"=>"Y",
				"ID"=>$this->arParams['WORKMAN_ID']
			],
			"select" => [
				'ID',
				'NAME',
				'LAST_NAME',
				'UF_CRM_POSITION'
			],
			'limit'=>1
		])->Fetch();

		$this->arResult['FORM_VALUES']=[
			'ACTION'=>'EDIT',
			'NAME'=>$arUser['NAME'].' '.$arUser['LAST_NAME'],
			'POSITION'=>$arUser['UF_CRM_POSITION'],
			'WORKMAN_ID'=>$arUser['ID']
		];

		//получаем роли
		$this->arResult['POSITION']=[];
		$arPositionField = CUserTypeEntity::GetList(
			[],
			[
				"FIELD_NAME"=>"UF_CRM_POSITION",
				"ENTITY_ID"=>"USER"
			]
		)->Fetch();
		$resPosition = CUserFieldEnum::GetList(
			["SORT"=>"ASC"],
			["USER_FIELD_ID" => $arPositionField["ID"]]
		);
		while($arPosition = $resPosition->Fetch()) {
			$this->arResult['POSITION'][$arPosition['ID']]=$arPosition;
		}

		return $this->showForm();
	}

	private function showForm() {
		//получаем роли
		$this->arResult['POSITION']=[];
		$arPositionField = CUserTypeEntity::GetList(
			[],
			[
				"FIELD_NAME"=>"UF_CRM_POSITION",
				"ENTITY_ID"=>"USER"
			]
		)->Fetch();
		$resPosition = CUserFieldEnum::GetList(
			["SORT"=>"ASC"],
			["USER_FIELD_ID" => $arPositionField["ID"]]
		);
		while($arPosition = $resPosition->Fetch()) {
			$this->arResult['POSITION'][$arPosition['ID']]=$arPosition;
		}

		$this->IncludeComponentTemplate();
		return null;
	}

    public function addAction($name,$email,$positionId) {
		$user = new CUser;
		$password = $this->generatePassword();
		list($name,$lastName)=$this->parseName($name);
		$userId = $user->Add([
			"NAME" => $name,
			"LAST_NAME" => $lastName,
			"EMAIL" => $email,
			"LOGIN" => $email,
			"ACTIVE" => "Y",
			"PASSWORD" => $password,
			"CONFIRM_PASSWORD" => $password,

			"UF_CRM_POSITION"=>$positionId
		]);

		if ((int)$userId>0) {
			$this->arResult['SUCCESS']=true;
			$this->arResult['MESSAGE']='Пользователь создан';
		} else {
			$this->arResult['SUCCESS']=false;
			$this->arResult['ERROR']=[
				$user->LAST_ERROR
			];
		}

		return $this->arResult;
	}

	public function editAction($workmanId,$name,$positionId) {

		list($name,$lastName)=$this->parseName($name);

		$user = new CUser;
		$dbRes=$user->Update(
			$workmanId,
			[
				"NAME" => $name,
				"LAST_NAME" => $lastName,

				"UF_CRM_POSITION"=>$positionId
			]
		);

		if($dbRes) {
			$this->arResult['SUCCESS']=true;
			$this->arResult['MESSAGE']='Пользователь изменен';
		} else {
			$this->arResult['SUCCESS']=false;
			$this->arResult['ERROR']=$dbRes->getErrorMessages();
		}
		return $this->arResult;
	}

	public function deleteAction($workmanId) {

    	//ищем задачи
		Bitrix\Main\Loader::includeModule('iblock');
		Bitrix\Main\Loader::includeModule('highloadblock');

		$arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => [
			"=NAME" => "Crm"
		]])->fetch();
		if(!class_exists("\\CrmTable")) {
			\Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
		}

		//нам все равно перебирать все задачи что бы удалить пользователя из них
		$resTasks=\CrmTable::getList([
			'filter'=>['UF_WORKMAN'=>$workmanId],
			'select'=>['ID','UF_WORKMAN']
		]);

		$tasks=[];
		$onlyWorkman=false;
		while ($arTask = $resTasks->fetch()) {
			$tasks[]=$arTask;
			if (count($arTask['UF_WORKMAN'])==1) {
				$onlyWorkman=true;
				break;
			}
		}

		if (!$onlyWorkman) {
			//обновляем задачи
			foreach ($tasks AS $arTask) {
				unset($arTask['UF_WORKMAN'][array_search($workmanId,$arTask['UF_WORKMAN'])]); //нужный элемент точно есть
				$dbRes = \CrmTable::update(
					$arTask['ID'],
					[
						'UF_WORKMAN'=>$arTask['UF_WORKMAN'],
					]
				);
			}

			//удаляем пользователя
			$dbRes=CUser::Delete($workmanId);

			if ($dbRes) {
				$this->arResult['SUCCESS']=true;
				$this->arResult['MESSAGE']='Пользователь удален';
			} else {
				$this->arResult['SUCCESS']=false;
				$this->arResult['ERROR']=['Ошибка удаления пользователя']; //CUser::Delete не возвращает ошибку
			}
		} else {
			$this->arResult['SUCCESS']=false;
			$this->arResult['ERROR']=['Нельзя удалить единственного исполнителя у задачи']; //CUser::Delete не возвращает ошибку
		}


		return $this->arResult;
	}

	private function generatePassword($length=10) {
		$arr = array('a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'r', 's',
			't', 'u', 'v', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'R', 'S',
			'T', 'U', 'V', 'X', 'Y', 'Z',
			'1', '2', '3', '4', '5', '6',
			'7', '8', '9', '0');
		$pass = "";
		for ($i = 0; $i < $length; $i++) {
			$index = rand(0, count($arr) - 1);
			$pass .= $arr[$index];
		}
		return $pass;
	}

	private function parseName($name) {
		$arName = explode(' ', trim($name));
		return [
			$arName[0],
			implode(
				' ',
				array_slice($arName, 1, count($arName)-1, true)
			)
		];
	}
}