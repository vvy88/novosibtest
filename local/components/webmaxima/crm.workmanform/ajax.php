<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CBitrixComponent::includeComponentClass("webmaxima:crm.workmanform");
$component = new WebmaximaCrmWorkmanForm();

if ($_REQUEST['ACTION']=='ADD') {
	$result=$component->addAction(
		$_REQUEST['NAME'],
		$_REQUEST['EMAIL'],
		$_REQUEST['POSITION']
	);
} if ($_REQUEST['ACTION']=='EDIT') {
	$result=$component->editAction(
		$_REQUEST['WORKMAN_ID'],
		$_REQUEST['NAME'],
		$_REQUEST['POSITION']
	);
} if ($_REQUEST['ACTION']=='DELETE') {
	$result=$component->deleteAction(
		$_REQUEST['WORKMAN_ID']
	);
}

echo json_encode($result);
exit;
