<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class WebmaximaCrmTasklist extends \CBitrixComponent {

    public function executeComponent() {
		Bitrix\Main\Loader::includeModule('iblock');
		Bitrix\Main\Loader::includeModule('highloadblock');

		$arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => [
			"=NAME" => "Crm"
		]])->fetch();
		if(!class_exists("\\CrmTable")) {
			\Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
		}

		$resTasks=\CrmTable::getList([
			'order'=>['ID'=>'ASC']
		]);

		$this->arResult['TASK']=[];
		$workManIds=[];
		while ($arTask = $resTasks->fetch()) {
			if (is_array($arTask['UF_WORKMAN'])) {
				$workManIds=array_merge($workManIds,$arTask['UF_WORKMAN']);
			}
			$this->arResult['TASK'][]=$arTask;
		}
		$workManIds=array_unique($workManIds);

		$this->arResult['TASK_WORKMAN']=[];
		$resUser = \Bitrix\Main\UserTable::getList([
			"filter" => [
				"ACTIVE"=>"Y",
				"=ID"=>$workManIds,
			],
			"select" => [
				'ID',
				'NAME',
				'LAST_NAME'
			]
		]);
		while ($arUser = $resUser->fetch()) {
			$this->arResult['TASK_WORKMAN'][$arUser['ID']]=$arUser;
		}

		$this->arResult['TASK_STATUS']=[];
		$arStatusField = CUserTypeEntity::GetList(
			[],
			[
				"FIELD_NAME"=>"UF_STATUS",
				"ENTITY_ID"=>"HLBLOCK_".$arHLBlock["ID"]
			]
		)->Fetch();
		$resStatus = CUserFieldEnum::GetList(
			["SORT"=>"ASC"],
			["USER_FIELD_ID" => $arStatusField["ID"]]
		);
		while($arStatus = $resStatus->Fetch()) {
			$this->arResult['TASK_STATUS'][$arStatus['ID']]=$arStatus;
		}

		$this->IncludeComponentTemplate();

        return $this->arResult;
    }

}