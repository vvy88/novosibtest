<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?
Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/assets/jBox/dist/jBox.all.min.js');
Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/assets/jBox/dist/jBox.all.min.css');
?>

<div id="taskListBlock">
    <table class="table" id="taskList">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Название</th>
            <th scope="col">Исполнитель</th>
            <th scope="col">Статус</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
        <? if (!count($arResult['TASK'])) { ?>
            <tr>
                <td colspan="5">Задачи не найдены</td>
            </tr>
        <? } else { ?>
            <? foreach ($arResult['TASK'] AS $arTask) { ?>
                <tr class="taskBlock">
                    <td><?=$arTask['ID']?></td>
                    <td><?=$arTask['UF_NAME']?></td>
                    <td>
                        <?
                        $workmanStr=[];
                        foreach ($arTask['UF_WORKMAN'] AS $userId) {
                            $workmanStr[]=$arResult['TASK_WORKMAN'][$userId]['NAME'].' '.$arResult['TASK_WORKMAN'][$userId]['LAST_NAME'];
                        } ?>
                        <?=implode(', ',$workmanStr);?>
                    </td>
                    <td>
                        <?=$arResult['TASK_STATUS'][$arTask['UF_STATUS']]['VALUE']?>
                    </td>
                    <td>
                        <a class="editTask" href="" data-href="/ajax/taskform.php?ACTION=EDIT_FORM&JS_CALLBACK=reloadTaskList&TASK_ID=<?=$arTask['ID']?>">редактировать</a>
                        <a class="deleteTask" href="#" data-href="/local/components/webmaxima/crm.taskform/ajax.php?ACTION=DELETE&TASK_ID=<?=$arTask['ID']?>">удалить</a>
                    </td>
                </tr>
            <? } ?>
        <? } ?>
        </tbody>
    </table>

    <a class="btn btn-primary" href="#" data-href='/ajax/taskform.php?ACTION=ADD_FORM&JS_CALLBACK=reloadTaskList' id="taskAdd" role="button">Добавить задачу</a>
</div>
