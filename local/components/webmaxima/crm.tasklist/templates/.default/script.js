$(document).ready(function(){
	//открываем окно с созданием задачи
	$(document).on('click', '#taskAdd', function(){
		new jBox('Modal')
			.setTitle('Создать задачу')
			.open({
				ajax : {url : $(this).attr('data-href')},
				width: 500,
			});
	});

	//открываем окно с редактированием задачи
	$(document).on('click', '#taskList .editTask', function(){
		new jBox('Modal')
			.setTitle('Редактирование задачи')
			.open({
				ajax : {url : $(this).attr('data-href')},
				width: 500,
			});

		return false;
	});

	//удаление задачи
	$(document).on('click', '#taskList .deleteTask', function(){
		var link=this;
		$.ajax({
			type: 'GET',
			url: $(this).attr('data-href'),
			dataType: 'json',
			success: function (data) {
				if (data.SUCCESS) {
					reloadTaskList(data);
					alert(data.MESSAGE);
				} else {
					alert(data.ERROR.join());
				}
			},
			error: function (xhr, str) {
				alert('Оу, что-то пошло не так, обратитесь в техническую поддержку :(');
			}
		});
		return false;
	});

});

function reloadTaskList(data) {
	if (data.SUCCESS) {
		$.ajax({
			type: 'GET',
			url: '/ajax/tasklist.php',
			success: function (data) {
				$('#taskListBlock').replaceWith(data)
			},
			error: function (xhr, str) {
				alert('Оу, что-то пошло не так, обратитесь в техническую поддержку :(');
			}
		});
	}
}