<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<form id="taskForm" data-action="<?=$arResult['AJAX_URL']?>" data-jsCallback="<?=$arResult['JS_CALLBACK']?>">
    <div class="message"></div>
    <input type="hidden" name="ACTION" value="<?=$arResult['FORM_VALUES']['ACTION']?>">

    <? if ($arResult['ACTION']=='EDIT_FORM') { ?>
        <input type="hidden" name="TASK_ID" value="<?=$arResult['FORM_VALUES']['TASK_ID']?>">
    <? } ?>

    <div class="row form-group">
        <div class="col-4">
            <label for="addFormName">Название</label>
        </div>
        <div class="col-8">
            <input type="text" class="form-control" id="addFormName" name="NAME" value="<?=$arResult['FORM_VALUES']['NAME']?>" required>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-4">
            <label for="addFormWorkman">Исполнитель</label>
        </div>
        <div class="col-8">
            <select multiple class="form-control" id="addFormWorkman" name="WORKMAN[]" required>
				<? foreach ($arResult['WORKMAN'] AS $arUser) { ?>
                    <option
                        value="<?=$arUser['ID']?>"
						<? if (in_array($arUser['ID'],$arResult['FORM_VALUES']['WORKMAN'])) {?> selected <?} ?>
                    ><?=$arUser['NAME']?> <?=$arUser['LAST_NAME']?></option>
				<? } ?>
            </select>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-4">
            <label for="addFormStatus">Статус</label>
        </div>
        <div class="col-8">
            <select class="form-control" id="addFormStatus" name="STATUS" required>
				<? foreach($arResult['TASK_STATUS'] AS $arStatus) { ?>
                    <option
                        value="<?=$arStatus['ID']?>"
						<? if ($arStatus['ID']==$arResult['FORM_VALUES']['STATUS']) {?> selected <?} ?>
                    ><?=$arStatus['VALUE']?></option>
				<? } ?>
            </select>
        </div>
    </div>

    <button type="submit" class="btn btn-success">Сохранить</button>
</form>

<? //не убирать в script.php подгружается через ajax ?>
<script>
	$(document).ready(function(){
		$('#taskForm').submit(function() {
			var form=this;
			var data   = $(this).serialize();
			$.ajax({
				type: 'POST',
				url: $(this).attr('data-action'),
				data: data,
				dataType: 'json',
				success: function (data) {
                    if (typeof window[$(form).attr('data-jsCallback')] == 'function') {
						window[$(form).attr('data-jsCallback')](data);
                    }
					if (data.SUCCESS) {
						$(form).replaceWith('<div class="alert alert-success" role="alert">'+data.MESSAGE+'</div>');
					} else {
						$(form).find('.message').html('<div class="alert alert-danger" role="alert">'+data.ERROR.join()+'</div>');
					}
				},
				error: function (xhr, str) {
					$(form).replaceWith('<div class="alert alert-danger" role="alert">Оу, что-то пошло не так, обратитесь в техническую поддержку :(</div>');
				}
			});

			return false;
		});
	});
</script>