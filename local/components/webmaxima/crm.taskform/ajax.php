<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CBitrixComponent::includeComponentClass("webmaxima:crm.taskform");
$component = new WebmaximaCrmTaskForm();

if ($_REQUEST['ACTION']=='ADD') {
	$result=$component->addAction(
		$_REQUEST['NAME'],
		$_REQUEST['WORKMAN'],
		$_REQUEST['STATUS']
	);
} if ($_REQUEST['ACTION']=='EDIT') {
	$result=$component->editAction(
		$_REQUEST['TASK_ID'],
		$_REQUEST['NAME'],
		$_REQUEST['WORKMAN'],
		$_REQUEST['STATUS']
	);
} if ($_REQUEST['ACTION']=='DELETE') {
	$result=$component->deleteAction(
		$_REQUEST['TASK_ID']
	);
}

echo json_encode($result);
exit;
