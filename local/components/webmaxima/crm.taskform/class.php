<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class WebmaximaCrmTaskForm extends \CBitrixComponent {
	private $arHLBlock=null; //данные о HL блоке

    public function executeComponent() {
    	$this->arResult['ACTION']=$this->arParams['ACTION'];
		$this->arResult['AJAX_URL']=$this->GetPath().'/ajax.php';
		$this->arResult['JS_CALLBACK']=$this->arParams['JS_CALLBACK'];

    	if (empty($this->arParams['ACTION']) || $this->arParams['ACTION']=='ADD_FORM') {
			return $this->showAddForm();
		} else if ($this->arParams['ACTION']=='EDIT_FORM') {
			return $this->showEditForm();
		}
    }

	private function showAddForm() {
		$this->arResult['FORM_VALUES']=[
			'ACTION'=>'ADD',
		];
    	return $this->showForm();
	}

	private function showEditForm() {
		$arHLBlock=$this->initHLBlock();

		//данные задачи
		$arTask=\CrmTable::getById($this->arParams['TASK_ID'])->Fetch();

		$this->arResult['FORM_VALUES']=[
			'ACTION'=>'EDIT',
			'NAME'=>$arTask['UF_NAME'],
			'WORKMAN'=>$arTask['UF_WORKMAN'],
			'STATUS'=>$arTask['UF_STATUS'],
			'TASK_ID'=>$arTask['ID']
		];
		return $this->showForm();
	}

	private function showForm() {
		$arHLBlock=$this->initHLBlock();

		//получаем статусы
		$this->arResult['TASK_STATUS']=[];
		$arStatusField = CUserTypeEntity::GetList(
			[],
			[
				"FIELD_NAME"=>"UF_STATUS",
				"ENTITY_ID"=>"HLBLOCK_".$arHLBlock["ID"]
			]
		)->Fetch();
		$resStatus = CUserFieldEnum::GetList(
			["SORT"=>"ASC"],
			["USER_FIELD_ID" => $arStatusField["ID"]]
		);
		while($arStatus = $resStatus->Fetch()) {
			$this->arResult['TASK_STATUS'][$arStatus['ID']]=$arStatus;
		}

		//получаем исполнителей
		$this->arResult['WORKMAN']=[];
		$resUser = \Bitrix\Main\UserTable::getList([
			"filter" => [
				"ACTIVE"=>"Y"
			],
			"order"=>['ID'=>'ASC'],
			"select" => [
				'ID',
				'NAME',
				'LAST_NAME',
				'UF_CRM_POSITION'
			]
		]);
		while ($arUser = $resUser->fetch()) {
			$this->arResult['WORKMAN'][$arUser['ID']]=$arUser;
		}

		$this->IncludeComponentTemplate();
		return null;
	}

    public function addAction($name,$workmanIds,$statusId) {
		$arHLBlock=$this->initHLBlock();

		//добавляем задачу
		$dbRes = \CrmTable::add([
			'UF_NAME'=>$name,
			'UF_WORKMAN'=>$workmanIds,
			'UF_STATUS'=>$statusId
		]);
		if($dbRes->isSuccess()) {
			$this->arResult['SUCCESS']=true;
			$this->arResult['MESSAGE']='Задача добавлена';
		} else {
			$this->arResult['SUCCESS']=false;
			$this->arResult['ERROR']=$dbRes->getErrorMessages();
		}

		return $this->arResult;
	}

	public function editAction($taskId,$name,$workmanIds,$statusId) {
		$arHLBlock=$this->initHLBlock();

		$dbRes = \CrmTable::update(
			$taskId,
			[
				'UF_NAME'=>$name,
				'UF_WORKMAN'=>$workmanIds,
				'UF_STATUS'=>$statusId
			]
		);
		if($dbRes->isSuccess()) {
			$this->arResult['SUCCESS']=true;
			$this->arResult['MESSAGE']='Задача изменена';
		} else {
			$this->arResult['SUCCESS']=false;
			$this->arResult['ERROR']=$dbRes->getErrorMessages();
		}
		return $this->arResult;
	}

	public function deleteAction($taskId) {
		$arHLBlock=$this->initHLBlock();

		$dbRes = \CrmTable::delete($taskId);
		if($dbRes->isSuccess()) {
			$this->arResult['SUCCESS']=true;
			$this->arResult['MESSAGE']='Задача удалена';
		} else {
			$this->arResult['SUCCESS']=false;
			$this->arResult['ERROR']=$dbRes->getErrorMessages();
		}
		return $this->arResult;
	}

	private function initHLBlock() {
		Bitrix\Main\Loader::includeModule('iblock');
		Bitrix\Main\Loader::includeModule('highloadblock');

    	if (is_null($this->arHLBlock)) {
			$arHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => [
				"=NAME" => "Crm"
			]])->fetch();
			if(!class_exists("\\CrmTable")) {
				\Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
			}
			$this->arHLBlock=$arHLBlock;
		}
		return $this->arHLBlock;
	}
}