<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<?
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/assets/jquery/jquery.min.js');

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/assets/bootstrap/js/bootstrap.min.js');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/assets/bootstrap/css/bootstrap.min.css');
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?$APPLICATION->ShowTitle();?></title>
	<?$APPLICATION->ShowHead();?>
</head>
<body>
<div class="maxwidth">