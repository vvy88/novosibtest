<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('Главная');
?>

<ul class="nav nav-tabs">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#task">Задачи</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#workman">Исполнители</a>
	</li>
</ul>
<div class="tab-content">
	<div class="tab-pane fade show active" id="task">
		<?$APPLICATION->IncludeComponent(
			"webmaxima:crm.tasklist",
			'',
			array(
			),
			false
		);?>
	</div>
	<div class="tab-pane fade " id="workman">
		<?$APPLICATION->IncludeComponent(
			"webmaxima:crm.workmanlist",
			'',
			array(
			),
			false
		);?>
	</div>
</div>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>